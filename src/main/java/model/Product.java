package model;

import java.util.List;
import java.util.Map;

/**
 * @author Joshua Rule
 */
public class Product {

	private String id;
	private String url;
	private String title;
	private String price;
	private String description;
	private List<String> categories;
	private String stockStatus;
	private Map<String, String> additionalDetails;

	public Product() {}

	public String getUrl() {
		return url;
	}

	public void setUrl(final String url) {
		this.url = url;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(final String title) {
		this.title = title;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(final String price) {
		this.price = price;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(final String description) {
		this.description = description;
	}

	public List<String> getCategories() {
		return categories;
	}

	public void setCategories(final List<String> categories) {
		this.categories = categories;
	}

	public String getStockStatus() {
		return stockStatus;
	}

	public void setStockStatus(final String stockStatus) {
		this.stockStatus = stockStatus;
	}

	public Map<String, String> getAdditionalDetails() {
		return additionalDetails;
	}

	public void setAdditionalDetails(final Map<String, String> additionalDetails) {
		this.additionalDetails = additionalDetails;
	}

	public String asText() {
		final StringBuilder output = new StringBuilder();

		output
				.append("url: ").append(url)
				.append("\nid: ").append(id)
				.append("\nTitle: ").append(title)
				.append("\nPrice: ").append(price)
				.append("\nDescription: ").append(description)
				.append("\nCategories: ");

		String delimiter = "";

		for (String category : categories) {
			output.append(delimiter).append(category);
			delimiter = ">";
		}

		additionalDetails.forEach((k, v) -> output
				.append("\n")
				.append(k)
				.append(": ")
				.append(v));

		return output.toString();
	}

	public void setId(final String id) {
		this.id = id;
	}

	public String getId() {
		return id;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Product product = (Product) o;

		if (id != null ? !id.equals(product.id) : product.id != null) return false;
		if (url != null ? !url.equals(product.url) : product.url != null) return false;
		if (title != null ? !title.equals(product.title) : product.title != null) return false;
		if (price != null ? !price.equals(product.price) : product.price != null) return false;
		if (description != null ? !description.equals(product.description) : product.description != null) return false;
		if (categories != null ? !categories.equals(product.categories) : product.categories != null) return false;
		if (stockStatus != null ? !stockStatus.equals(product.stockStatus) : product.stockStatus != null) return false;
		return additionalDetails != null ? additionalDetails.equals(product.additionalDetails) : product.additionalDetails == null;

	}

	@Override
	public int hashCode() {
		int result = id != null ? id.hashCode() : 0;
		result = 31 * result + (url != null ? url.hashCode() : 0);
		result = 31 * result + (title != null ? title.hashCode() : 0);
		result = 31 * result + (price != null ? price.hashCode() : 0);
		result = 31 * result + (description != null ? description.hashCode() : 0);
		result = 31 * result + (categories != null ? categories.hashCode() : 0);
		result = 31 * result + (stockStatus != null ? stockStatus.hashCode() : 0);
		result = 31 * result + (additionalDetails != null ? additionalDetails.hashCode() : 0);
		return result;
	}
}
