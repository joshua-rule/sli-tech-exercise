package builder;

import model.Product;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Joshua Rule
 */
public class ProductPageExtractor {

	public static Product getProduct(final String productUrl) throws Exception {
		System.out.println("Fetching product: " + productUrl);

		final Document productPage = Jsoup.connect(productUrl).get();
		final List<Element> productElements = productPage.select(".product-essential");

		if (productElements.size() > 1 || productElements.size() == 0) {
			throw new Exception("Failed to grab only one product, got: " + productElements.size());
		}

		final Element productElement = productElements.get(0);
		final Product product = new Product();

		product.setUrl(productUrl);
		product.setId(productElement.select("form").get(0).attr("action").replaceAll("^.*?product/", "").replaceAll("/form_key.*?$", ""));
		product.setTitle(getElementText(productElement, ".product-name > span"));
		product.setStockStatus(getElementText(productElement, ".availability > .value"));
		product.setPrice(getElementText(productElement, ".price-info .price"));
		product.setDescription(getElementText(productElement, ".short-description .std"));
		product.setAdditionalDetails(getAdditionalDetails(productPage));

		// TODO: Doesn't always work, seems like the server often doesn't return any BCT info other than home > product-name
		final List<Element> bct = productPage.select(".breadcrumbs ul li");
		final List<String> categories = new ArrayList<>();

		// remove the first and last as they aren't categories
		bct.remove(0);
		bct.remove(bct.size()-1);

		bct.forEach(row ->
				categories.add(row.select("a").get(0).ownText())
		);

		product.setCategories(categories);

		System.out.println("Successfully fetched product: " + productUrl);

		return product;
	}

	private static String getElementText(final Element productElement, final String querySelector) {
		final List<Element> element = productElement.select(querySelector);

		if (element.size() == 0) {
			return null;
		} else {
			return element.get(0).ownText();
		}
	}

	private static Map<String, String> getAdditionalDetails(final Document productPage) {
		final Map<String, String> additionalDetails = new HashMap<>();
		final Element collateralTabs = productPage.getElementById("collateral-tabs");

		if (collateralTabs == null) {
			return additionalDetails;
		}

		final List<Element> tabContainer = collateralTabs.select(".tab-container");

		if (tabContainer.size() < 2) {
			return additionalDetails;
		}

		final List<Element> tableBody = tabContainer.get(1).select("tbody");

		if (tableBody.size() == 0) {
			return additionalDetails;
		}

		final Element additionalDetailsTable = tableBody.get(0);

		final List<Element> rows = additionalDetailsTable.select("tr");

		rows.forEach(row -> additionalDetails.put(
				row.select("th").get(0).ownText(),
				row.select("td").get(0).ownText()
		));

		return additionalDetails;
	}
}
