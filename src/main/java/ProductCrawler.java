import builder.ProductPageExtractor;

import com.google.common.base.Strings;
import model.Product;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import rx.Observable;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Instant;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Joshua Rule
 */
public class ProductCrawler {

	private static final String FEED_OUTPUT_LOCATION = "/feeds/";
	private static final String HOME_PAGE = "http://www.sli-demo.com";
	private static final Map<String, Product> PRODUCTS = new HashMap<>();

	public static void main(final String[] args) throws Exception {
		if (args.length == 0 || "crawl".equals(args[0])) {
			findProducts();
			outputProductCatalog();
		} else if (args.length > 0) {
			final Product product = ProductPageExtractor.getProduct(args[0]);
			PRODUCTS.put(product.getId(), product);
			outputProductCatalog();
		}
	}

	private static void findProducts() throws IOException {
		final Document homePage = Jsoup.connect(HOME_PAGE).get();
		final List<Element> parentLists = homePage.select("#nav .nav-primary > li");

		Observable.from(parentLists)
				.flatMap(parentList -> Observable.from(parentList.select("li > a")))
				.map(categoryElement -> categoryElement.attr("href"))
				.filter(link -> !Strings.isNullOrEmpty(link))
				.flatMap(ProductCrawler::getProductLinks)
				.map(productElement -> productElement.attr("href"))
				.filter(link -> !Strings.isNullOrEmpty(link))
				.map(productLink -> {
					try {
						return ProductPageExtractor.getProduct(productLink);
					} catch (Exception e) {
						e.printStackTrace();
						return null;
					}
				})
				.filter(product -> product != null)
				.subscribe(ProductCrawler::insertProduct);
	}

	private static Observable<Element> getProductLinks(final String url) {
		System.out.println("Fetching category page: " + url);
		Document page = null;

		try {
			page = Jsoup.connect(url).get();
		} catch (IOException e) {
			e.printStackTrace();
		}

		if (page != null) {
			System.out.println("Successfully fetched category page: " + url);
			return Observable.from(page.select(".category-products ul.products-grid > li > a"));
		} else {
			System.out.println("Failed to fetch category page: " + url);
			return null;
		}
	}

	private static void insertProduct(final Product product) {
		PRODUCTS.put(product.getId(), product);
	}

	private static void outputProductCatalog() throws IOException {
		final StringBuilder stringBuilder = new StringBuilder();
		final Path outputDir = Paths.get(FEED_OUTPUT_LOCATION);
		final Path outputPath = outputDir.resolve("feed-" + Instant.now().getEpochSecond() + ".txt");

		if (! Files.exists(outputDir)) {
			Files.createDirectory(outputDir);
		}

		if (Files.exists(outputPath)) {
			throw new IOException("File already exists with path: " + outputPath);
		} else {
			Files.createFile(outputPath);
		}

		System.out.println("Outputting feed to: " + outputPath.toAbsolutePath().toString());

		PRODUCTS.forEach((k, v) -> stringBuilder.append(v.asText()).append("\n"));

		try (final FileWriter writer = new FileWriter(outputPath.toFile())) {
			writer.write(stringBuilder.toString());
		}
	}
}
