package builder;

import com.github.tomakehurst.wiremock.junit.WireMockRule;
import com.google.common.io.CharStreams;
import model.Product;
import org.junit.Rule;
import org.junit.Test;

import java.io.*;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static java.nio.charset.StandardCharsets.UTF_8;
import static org.junit.Assert.assertEquals;

/**
 * @author Joshua Rule
 */
public class ProductPageExtractorUnitTest {

	private final static int PORT = 8089;
	private final static String BASE_URL = "http://localhost:" + PORT;

	@Rule
	public WireMockRule wireMockRule = new WireMockRule(PORT);

	@Test
	public void testFetchingAProductFromAPage() throws Exception {
		final String testUrl = "/product.html";
		final String testBody = getFileAsString("product.html");

		stubFor(
				get(urlEqualTo("/product.html"))
						.willReturn(aResponse()
								.withStatus(200)
								.withBody(testBody)));

		final Product product = ProductPageExtractor.getProduct(BASE_URL + testUrl);

		assertEquals(BASE_URL + testUrl, product.getUrl());
		assertEquals("Herald Glass Vase", product.getTitle());
		assertEquals("$110.00", product.getPrice());
		assertEquals("In stock", product.getStockStatus());
		assertEquals("The uniquely shaped Herand Glass Vase packs easily and adds instant impact.", product.getDescription());
		assertEquals("Glass", product.getAdditionalDetails().get("Material"));
		assertEquals("No", product.getAdditionalDetails().get("Bed & Bath Type"));
		assertEquals("Vase", product.getAdditionalDetails().get("Decor Type"));
		assertEquals("No", product.getAdditionalDetails().get("Color"));
		assertEquals("Home & Decor", product.getCategories().get(0));
		assertEquals("Decorative Accents", product.getCategories().get(1));
	}

	private String getFileAsString(final String fileName) throws IOException {
		try (final Reader reader = new InputStreamReader(getClass().getResourceAsStream(fileName), UTF_8)) {
			return CharStreams.toString(reader);
		}
	}

}
